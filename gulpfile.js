// requires
const { src, dest, watch, parallel, series } = require('gulp');
// var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const del = require('del');
// const runSequence = require('run-sequence'); // очередь команд
// var autoprefixerr = require('autoprefixer');
const autoprefixer = require('gulp-autoprefixer');
const basePaths = {
	src: 'source',
	dest: 'f',
	delete: 'f/css/main.css'
};

const cdPaths = {
	// cssModules: [basePaths.src + '/var.scss', basePaths.modules + '/**/*.scss'],
	style: basePaths.src + '/style'
};
const cdDest = {
	style: basePaths.dest+'/css'
};

const cdWatch = {
	css: cdPaths.style + '/**/*.css',
	scss: cdPaths.style + '/**/*.scss',
};



let onError = function (err) {
	console.log(err);
	this.emit('end');
};

function styles() {
	return src([`${cdPaths.style}/var.scss`, `${cdPaths.style}/**/*.scss`])
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
		.pipe(dest(cdDest.style));
};

function clean() {
	return del( basePaths.delete, { force: true } );
};

// function builder(){
// 	runSequence('clean',
// 		['sass'],
// 		function() {
// 			console.log('Build finished --> \n');
// 		}
// 	);
// };

function watcher(){
	watch([cdWatch.css, cdWatch.scss], styles);
};


exports.default = parallel(styles, watcher);
exports.styles = styles;
exports.clean = clean;
exports.watch = watcher;
exports.build = series(clean, styles);
