'use strict';

window.addEventListener('load', function () {
/**
 * @param MODEL_BASE{
 *      width: number, // width BOARD
 *      height: number, // height BOARD
 *      zoom: number, // default ZOOM board
 *      widthBlock: number, // default BLOCK width
 *      heightBlock: number, // default BLOCK height
 *      blockColor: '#FFFFFF', // background BLOCK
 *      blockBorder: '#333333', // color border BLOCK
 *      fontSizeBlock: number, // font-size TEXT in BLOCK
 *      fontSizeArrow: number, // font-size TEXT in ARROW
 * }
 * @param MODEL_BLOCK{
 *      type: string('BLOCK'),
 *      id: string(TIMESTAMP),
 *      x: number(PX),
 *      y: number(PX),
 *
 *      text?: string
 * }
 * @param MODEL_ARROW{
 *      type: string('ARROW'),
 *      id: string(TIMESTAMP),
 *      points: string([start.coord], [end.coord]),
 *
 *      text?: string
 *
 *      vector: string('UP', 'DOWN', 'LEFT', 'RIGHT')
 *      idNodeIn: string,
 *      idNodeOut: string
 * }
 * @param MODEL_ARROW_ID_BY_BLOCK{
 *     idNode: string; ID_BLOCK;
 *     vectorHandle: string; - top|right|bottom|left
 * }
 * @param MODEL_CURRENT_DRAG_ELEMENT{
 *      eventTarget: [],
 *      eventMouse:[],
 *      cursorLambdaX: 0,
 *      cursorLambdaY: 0
 *
 *      arrowVectorStart: top|right|bottom|left
 * }
  */
const schemeSVG = {
    originalModel: {}, // оригинальная модель(боевая, результативная, итоговая)
    model: {}, // временная модель
    scheme: [], //NODE scheme SVG, main board
    schemePoligon: null, // NODE poligon
    schemeNSnameSpace: 'http://www.w3.org/2000/svg',
    shemeDragElem: { },
    controlsID: {
        block:'scheme-add-block',
        arrow: 'scheme-add-arrow',
        clear: 'scheme-clear'
    },
    controls:{ // кнопки контролов
        block: [],
        arrow: [],
        clear: []
    },
    defaultModelBaseClass: class {  // @param MODEL_BLOCK
        constructor(
            width,
            height,
            zoom,
            widthBlock,
            heightBlock,
            blockColor,
            blockBorder,
            fontSizeBlock,
            fontSizeArrow
        ) {
            this.width = width;
            this.height = height;
            this.zoom = zoom;
            this.widthBlock = widthBlock;
            this.heightBlock = heightBlock;
            this.blockColor = blockColor;
            this.blockBorder = blockBorder;
            this.fontSizeBlock = fontSizeBlock;
            this.fontSizeArrow = fontSizeArrow;
        }
    },
    defaultModelBlockClass: class {  // @param MODEL_BLOCK
        constructor(id, x, y) {
            this.type = 'BLOCK';
            this.arrowIdIn = [];
            this.arrowIdOut = [];
            this.id = String(Date.now());
            this.x = x;
            this.y = y;
        }
    },
    defaultModelArrowClass: class {  // @param MODEL_ARROW
        constructor(points, idNodeOut, idNodeIn) {
            this.type = 'ARROW';
            this.idNodeOut = idNodeOut;
            this.idNodeIn = idNodeIn;
            this.id = String(Date.now());
            this.points = points;
        }
    },
    arrowIdByBLockClass: class {  // @param MODEL_ARROW_ID_BY_BLOCK
        constructor(idNode, vectorHandle) {
            this.idNode = idNode;
            this.vectorHandle = vectorHandle;
        }
    },
    defaultModelDragElementClass: class {  // @param MODEL_CURRENT_DRAG_ELEMENT
        constructor(eventTarget, eventMouse, cursorLambdaX, cursorLambdaY, isChanged) {
            this.eventTarget = eventTarget;
            this.eventMouse = eventMouse;
            this.cursorLambdaX = cursorLambdaX;
            this.cursorLambdaY = cursorLambdaY;
            this.isChanged = isChanged || false;
        }
    },
    /** Инициализируем приложение */
    init: function () {
        schemeSVG.scheme = document.getElementById('or-scheme-container');
        schemeSVG.schemePoligon = document.getElementById('scheme-poligon');
        schemeSVG.controls.block = document.getElementById(schemeSVG.controlsID.block);
        schemeSVG.controls.clear = document.getElementById(schemeSVG.controlsID.clear);
        // schemeSVG.controls.arrow = document.getElementById(schemeSVG.controlsID.arrow);
        console.info('# [schemeSVG] - INIT');

        if(schemeSVG.scheme && schemeSVG.schemePoligon && schemeSVG.controls.block
            // && schemeSVG.controls.arrow
        ){
            schemeSVG.addEventListener();
            schemeSVG.afterInit();
        }
    },
    /** Слушатели */
    addEventListener(){
        if(schemeSVG.controls.block
            // && schemeSVG.controls.arrow
        ){
            schemeSVG.controls.block.removeEventListener('click', schemeSVG.onAddBlock);
            schemeSVG.controls.block.addEventListener('click', schemeSVG.onAddBlock);

            // schemeSVG.controls.arrow.removeEventListener('click', schemeSVG.onAddArrow)
            // schemeSVG.controls.arrow.addEventListener('click', schemeSVG.onAddArrow)

            schemeSVG.controls.clear.removeEventListener('click', schemeSVG.onClear)
            schemeSVG.controls.clear.addEventListener('click', schemeSVG.onClear)

            //TODO - тестовый кейс
            document.getElementById('scheme-add-custom').removeEventListener('click', schemeSVG.test)
            document.getElementById('scheme-add-custom').addEventListener('click', schemeSVG.test)
        }
    },
    /** Тестовая схема */
    test(){
        console.info('@@@@@ Тестовые данные');
        /** @param MODEL_BLOCK{
         *      type: string('BLOCK'),
         *      id: string(TIMESTAMP),
         *      x: number(PX),
         *      y: number(PX),
         *
         *      text?: string
         * }
         * @param MODEL_ARROW{
         *      type: string('ARROW'),
         *      id: string(TIMESTAMP),
         *      points: string([start.coord], [end.coord]),
         *
         *      text?: string
         *
         *      vector: string('UP', 'DOWN', 'LEFT', 'RIGHT')
         *      idNodeIn: string,
         *      idNodeOut: string
         * }
         */
        schemeSVG.onClear();
        let cTestModel = Object.assign({}, schemeSVG.model);
        cTestModel.node = Object.assign(
            cTestModel.node,
            [
                {
                    "type": "BLOCK",
                    "x": 140,
                    "y": 200,
                    "id": "1653384567959",
                    "text": "Тест-1",
                    "arrowIdOut": [{idNode: "1653384569588", vectorHandle: "right"}]
                },
                {
                    "type": "BLOCK",
                    "x": 442,
                    "y": 290,
                    "id": "1653384567960",
                    "text": "Тест-2",
                    "arrowIdIn": [{idNode: "1653384569588", vectorHandle: "left"}, {idNode:1653402689915,vectorHandle:"top"}],
                    "arrowIdOut": []
                },
                {
                    "type": "BLOCK",
                    "x": 382,
                    "y": 80,
                    "id": "1653384567961",
                    "text": "Тест-2",
                    "arrowIdIn": [],
                    "arrowIdOut": [{idNode: "1653402689915", vectorHandle: "bottom"}]
                },
                {
                    "type": "ARROW",
                    "id": "1653384569588",
                    "points": "239 250,442 341",
                    "idNodeOut": "1653384567959",
                    "idNodeIn": "1653384567960"
                },
                {
                    "type": "ARROW",
                    "id": "1653402689915",
                    "points": "432 179,492 292",
                    "idNodeOut": "1653384567961",
                    "idNodeIn": "1653384567960"
                }
            ]
        )
        schemeSVG.updateAllModels(cTestModel);
        schemeSVG.renderView(cTestModel);
    },
    /** Установка слушателей handler(ручек) внутри блока */
    addEventListenerNode(cNode){
        cNode.removeEventListener('mousedown', schemeSVG.onDragStartBlock)
        cNode.addEventListener('mousedown', schemeSVG.onDragStartBlock, false )
        const cHandlers = cNode.querySelectorAll('.or-scheme-item_handler');
        if(cHandlers.length){
            for(let i=0; i<cHandlers.length;i++){
                schemeSVG.addEventListenerHandler(cHandlers[i]);
            }
        }
    },
    /** Слушатели handler(ручек) внутри блока */
    addEventListenerHandler(cHandler){
        cHandler.removeEventListener('mousedown', schemeSVG.onArrowMousedown );
        cHandler.addEventListener('mousedown', schemeSVG.onArrowMousedown, false );

        cHandler.removeEventListener('mouseup', schemeSVG.onArrowMouseUp)
        cHandler.addEventListener('mouseup', schemeSVG.onArrowMouseUp);
    },

    /** После инициализации */
    afterInit(){
        schemeSVG.resetDragElem();
        schemeSVG.resetSchemePoligon();
        if(localStorage.getItem('or-scheme')) {
            schemeSVG.updateModel(JSON.parse(localStorage.getItem('or-scheme')));
            schemeSVG.renderView(schemeSVG.model);
        } else {
            schemeSVG.model.base = new schemeSVG.defaultModelBaseClass(
                schemeSVG.scheme.clientWidth,
                schemeSVG.scheme.clientHeight,
                1,
                100,
                100,
                '#FFFFFF',
                '#333333',
                16,
                12
            )
            schemeSVG.model.node = [];
            schemeSVG.updateModelLS();
        };
    },


    //  ----------------------- Model methods
    /** Обновление всей модели */
    updateModel(cModel){
        Object.assign(schemeSVG.model, cModel);
        console.info('# [schemeSVG] - updateModel');
    },
    /** Обноваление модели в localStore */
    updateModelLS(){
        localStorage.setItem('or-scheme', JSON.stringify(schemeSVG.model));
        console.info('# [schemeSVG] - updateModelLS');
    },
    /** Обноваление всех моделей */
    updateAllModels(cNewModel, isOnlyNodeUpdate){
        let currentModel = Object.assign({},schemeSVG.model);
        if(cNewModel){
            if(isOnlyNodeUpdate){
                let cFilterModelItem = currentModel.node.filter(item => item.id === cNewModel.id);
                let newModelNode = Object.assign([],currentModel.node);
                if(cFilterModelItem.length){
                    currentModel.node = newModelNode.map((item)=>{
                        if(item.id === cNewModel.id){ Object.assign(item, cNewModel) }
                        return item;
                    })
                } else{
                    newModelNode.push(Object.assign({},cNewModel));
                    currentModel.node = newModelNode;
                }
            }
            schemeSVG.updateModel(currentModel);
            schemeSVG.updateModelLS(currentModel);
            console.info('# [schemeSVG] - updateALLModel');
        }
    },
    /**
     * Обновление оригинальной модели.
     * Может быть использована при работе с временными моделями и обновлениями через сервер.
     * Можно вывести в интерфейс кнопки СОХРАНИТЬ и СБРОСИТЬ
     * Сохранить - обновляет оригинальную модель и сохраняет на сервере.
     * СБРОСИТЬ - возвращает состояние временной модели к последней сохранённой/загруженной оригинальной модели
     * */
    updateOrigModel(model){
        let cNewOrigModel = Object.assign({}, model);
        schemeSVG.originalModel = cNewOrigModel;
    },

    //  ----------------------- Views methods
     /**  очищаем полигон */
    resetSchemePoligon(){
        if(schemeSVG.schemePoligon){
            schemeSVG.schemePoligon.innerHTML = '';
        }
    },
    /** Отрисовка */
    renderView(cModel){
        schemeSVG.resetSchemePoligon(); // очищаем полигон
        if(cModel.node.length){
            for(let itemNode of cModel.node){
                schemeSVG.renderBlockByBoard(itemNode);
            }
        }
        console.info('# [schemeSVG] - renderView');
    },
    /**
     * Возвращает HTML-NODE - блок или группу блок+текст, или NULL
     * @param cModelBlock{
     * type: string('BLOCK'),
     * x: number(PX),
     * y: number(PX),
     * id: string(TIMESTAMP),
     * text: string
     * }
     */
    generateBlock(cModelBlock){
        let newGROUP = document.createElementNS(schemeSVG.schemeNSnameSpace, 'g'); // Create a <g>
        newGROUP.setAttribute('fill', schemeSVG.model.base.blockColor);
        newGROUP.setAttribute('stroke-width', '1');
        newGROUP.setAttribute('stroke', schemeSVG.model.base.blockBorder);
        newGROUP.setAttribute('class', 'or-scheme-item or-scheme-item_block');
        newGROUP.setAttribute('id', cModelBlock.id);
        newGROUP.setAttribute(
            'transform',
            `matrix(1, 0, 0, 1, ${parseInt(cModelBlock.x)}, ${parseInt(cModelBlock.y)})`
        );

        if( cModelBlock.x && cModelBlock.y && cModelBlock.id ){
            let newRECT = document.createElementNS(schemeSVG.schemeNSnameSpace, 'rect'); // Create a <rect>
            newRECT.setAttribute('x', 0);
            newRECT.setAttribute('y', 0);
            newRECT.setAttribute('width', schemeSVG.model.base.widthBlock);
            newRECT.setAttribute('height', schemeSVG.model.base.heightBlock);
            newGROUP.appendChild(newRECT);

            for(let itemVector of ['left', 'top', 'right', 'bottom']) {
                let newBlockHandler = document.createElementNS(schemeSVG.schemeNSnameSpace, 'circle'); // Create a <rect>
                newBlockHandler.setAttribute('r', '8');
                newBlockHandler.setAttribute('class', `or-scheme-item or-scheme-item_handler _${itemVector}`);
                newBlockHandler.setAttribute('or-vector', itemVector);
                newBlockHandler.setAttribute('cx', 0);
                newBlockHandler.setAttribute('cy', 0);
                switch (itemVector){
                    case 'left':
                        newBlockHandler.setAttribute(
                            'transform',
                            `matrix(1, 0, 0, 1, 0, ${parseInt(schemeSVG.model.base.heightBlock/2)+1} )`
                        );
                        break;
                    case 'top':
                        newBlockHandler.setAttribute(
                            'transform',
                            `matrix(1, 0, 0, 1, ${parseInt(schemeSVG.model.base.widthBlock/2)}, 2)`
                        );
                        break;
                    case 'right':
                        newBlockHandler.setAttribute(
                            'transform',
                            `matrix(1, 0, 0, 1, ${parseInt(schemeSVG.model.base.widthBlock)-1}, ${parseInt(schemeSVG.model.base.heightBlock/2)})`
                        );
                        break;
                    case 'bottom':
                        newBlockHandler.setAttribute(
                            'transform',
                            `matrix(1, 0, 0, 1, ${parseInt(schemeSVG.model.base.widthBlock/2)}, ${parseInt(schemeSVG.model.base.heightBlock) - 1})`
                        );
                        break;
                }
                newGROUP.appendChild(newBlockHandler);
            }

            return newRECT ? newGROUP : null;
        }
        return null;
    },
    /**
     * Возвращает HTML-NODE - вектор или группу вектор+текст, или NULL
     * @param cModelArrow
     *      type: string('ARROW'),
     *      x: number(PX),
     *      y: number(PX),
     *      id: string(TIMESTAMP),
     *      text: string
     *
     *      vector: string('UP', 'DOWN', 'LEFT', 'RIGHT')
     *      idNodeIn: string,
     *      idNodeOut: string
     * }
     */
    generateArrow(cModelArrow){
        if( cModelArrow.points && cModelArrow.id ){
            let newARROW = document.createElementNS(schemeSVG.schemeNSnameSpace, 'polyline'); // Create a <rect>
            // if( cModelArrow.x && cModelArrow.y && cModelArrow.width && cModelArrow.height && cModelArrow.id ){
            newARROW.setAttribute('fill', schemeSVG.model.base.blockColor);
            newARROW.setAttribute('stroke-width', '2');
            newARROW.setAttribute('stroke', schemeSVG.model.base.blockBorder);
            newARROW.setAttribute('class', 'or-scheme-item or-scheme-item_arrow');
            newARROW.setAttribute('id', cModelArrow.id);
            newARROW.setAttribute('points', cModelArrow.points);
            newARROW.setAttribute('transform', 'matrix(1,0,0,1,0,-2)');
            return newARROW ? newARROW : null;
        }
        return null
    },
    /** Генерирация <text>, возвращает HTML-NODE или NULL*/
    generateText(cModelBlock) {
        if( cModelBlock.x && cModelBlock.y && cModelBlock.id ){
            let newTEXT = document.createElementNS(schemeSVG.schemeNSnameSpace, 'text'); // Create a <text>
            newTEXT.setAttribute('x', cModelBlock.x);
            newTEXT.setAttribute('y', cModelBlock.y);

            newTEXT.setAttribute('length', '8');
            newTEXT.setAttribute('font-size', schemeSVG.model.base.fontSizeBlock);
            newTEXT.setAttribute('id', cModelBlock.id);
            newTEXT.setAttribute('text-anchor', 'middle');
            newTEXT.setAttribute('fill', schemeSVG.model.base.blockBorder);
            newTEXT.setAttribute('stroke', schemeSVG.model.base.blockBorder);

            newTEXT.setAttribute('dx', `calc(${schemeSVG.model.base.widthBlock}/2)`);
            newTEXT.setAttribute('dy', `calc(${schemeSVG.model.base.heightBlock}/2)`);

            let newTextItem = document.createTextNode(cModelBlock.text);
            newTEXT.appendChild(newTextItem);
            return newTEXT;
        }
        return null;
    },
    /**
     Отрисовка модели BLOCK в SVG
     */
    renderBlockByBoard(modelNODE){
        console.info('# [schemeSVG] - renderBlockByBoard');
        let resultNODE = null;
        if(modelNODE.type==='BLOCK'){
            resultNODE = schemeSVG.generateBlock(modelNODE);
        }
        if(modelNODE.type==='ARROW'){
            resultNODE = schemeSVG.generateArrow(modelNODE);
        }
        if(resultNODE){
            if(modelNODE.type==='ARROW'){
                schemeSVG.schemePoligon.prepend(resultNODE); // чтобы стрелки не заходили под блоки
            } else{
                schemeSVG.schemePoligon.appendChild(resultNODE);
                schemeSVG.addEventListenerNode(resultNODE);
            }

        }
    },
    /**
     * Render SVG - polilyne ARROW(line)
     * @param cModelArrow{
     * type: string('ARROW'),
     *      x: number(PX),
     *      y: number(PX),
     *      id: string(TIMESTAMP),
     *      text: string
     *
     *      vector?: string('UP', 'DOWN', 'LEFT', 'RIGHT')
     *      idNodeIn: string,
     *      idNodeOut: string
     * }
     */
    renderArrow(event){
        event.stopPropagation();
        let mouseCoord = schemeSVG.getMousePosition(event);
        let newPoints = `${schemeSVG.shemeDragElem.arrowRect.x} ${schemeSVG.shemeDragElem.arrowRect.y}, ${mouseCoord.x} ${mouseCoord.y}`;
        schemeSVG.shemeDragElem.arrowModel.points = newPoints;
        schemeSVG.shemeDragElem.arrowElement.setAttributeNS(null,'points',newPoints);
    },
    /** Отрисовка прилипания стрелок к блокам */
    spyArrow(arrowId, coords){
        let arrowElem = document.getElementById(arrowId);
        if(arrowElem){ arrowElem.setAttributeNS(null, 'points', coords) }
    },

    //  ----------------------- Controller methods
    /** Очистить модель */
    onClear(){
        if(schemeSVG.model.node.length){
            schemeSVG.model.node = [];
            schemeSVG.updateAllModels(schemeSVG.model);
            // schemeSVG.updateModel(schemeSVG.model);
            // schemeSVG.updateModelLS(schemeSVG.model);
            schemeSVG.renderView(schemeSVG.model);
            console.info('# [schemeSVG] - clear poligon' );
        }
    },
    /** Создаём блок с моделью DEFAULT на схеме и отрисовываем */
    onAddBlock: function (event){
        event.preventDefault();
        let newBlock = new schemeSVG.defaultModelBlockClass(
            Date.now(),
            parseInt(schemeSVG.scheme.getClientRects()[0].width/2 - schemeSVG.model.base.widthBlock/2),
            parseInt(schemeSVG.scheme.getClientRects()[0].height/2- schemeSVG.model.base.heightBlock/2)
        )
        schemeSVG.model.node.push(newBlock);
        schemeSVG.renderBlockByBoard(newBlock);
        schemeSVG.updateModelLS(schemeSVG.model);
    },
    /** Добавление  стрелки */
    // onAddArrow: function (event){
    //     event.preventDefault();
    //     let newArrow = Object.assign({
    //         id: Date.now(),
    //         points: '39 180, 580 145'
    //     }, schemeSVG.defaultModelArrow);
    //
    //     schemeSVG.updateAllModels(newArrow, true)
    //     schemeSVG.renderBlockByBoard(newArrow);
    // },
    /** Начало перемещения блока - нажатие мыши */
    onDragStartBlock(event){
        event.preventDefault();
        event.stopPropagation();

        let cRect = event.target.getBoundingClientRect();

        schemeSVG.shemeDragElem.eventMouse = event;
        schemeSVG.shemeDragElem.eventTarget = event.currentTarget;
        schemeSVG.shemeDragElem.cursorLambdaX = event.clientX - cRect.left;
        schemeSVG.shemeDragElem.cursorLambdaY = event.clientY - cRect.top;
        schemeSVG.shemeDragElem.idElement = schemeSVG.shemeDragElem.eventTarget.getAttributeNS(null, 'id');

        schemeSVG.scheme.removeEventListener('mousemove', schemeSVG.movieBlock);
        schemeSVG.scheme.addEventListener('mousemove', schemeSVG.movieBlock)
        schemeSVG.shemeDragElem.eventTarget.removeEventListener('mouseup', schemeSVG.onDragEndBlock, false);
        schemeSVG.shemeDragElem.eventTarget.addEventListener('mouseup', schemeSVG.onDragEndBlock, false);
    },
    /**  Окончание перемещения блока - отпустили мышь */
    onDragEndBlock(event){
        event.preventDefault();
        if(schemeSVG.shemeDragElem.eventTarget){
            const idBlock = schemeSVG.shemeDragElem.eventTarget.getAttributeNS(null, 'id');
            const cTransform  = schemeSVG.shemeDragElem.eventTarget.transform.baseVal.getItem(0);
            if(
                idBlock &&
                schemeSVG.model.node.filter(itemModel =>  itemModel.id === idBlock)
            ){
                schemeSVG.model.node.map(modelItem => {
                    if(modelItem.id == idBlock){
                        modelItem.x = cTransform.matrix.e;
                        modelItem.y = cTransform.matrix.f;
                    }
                    return modelItem;
                } )
            }

            schemeSVG.shemeDragElem.eventTarget.removeEventListener('mouseup', schemeSVG.onDragEndBlock);
            if(schemeSVG.shemeDragElem.isChanged){
                schemeSVG.shemeDragElem.isChanged = false;
                schemeSVG.resetDragElem();
                schemeSVG.updateModelLS();
            }
        }
        schemeSVG.scheme.removeEventListener('mousemove', schemeSVG.movieBlock);
    },
    /**  Вычисляем значение курсора */
    getMousePosition(event) {
        const CTM = event.currentTarget.getScreenCTM();
        return {
            x: (event.clientX - CTM.e) / CTM.a,
            y: (event.clientY - CTM.f) / CTM.d
        };
    },
    /** Перемещение блока */
    movieBlock(event){
        event.preventDefault();
        event.stopPropagation();
        let coord = schemeSVG.getMousePosition(event);
        let cTransform  = schemeSVG.shemeDragElem.eventTarget.transform.baseVal.getItem(0)
        let cMatrix = cTransform.matrix;
        let cNewMatrixX = coord.x - schemeSVG.shemeDragElem.cursorLambdaX;
        let cNewMatrixY = coord.y - schemeSVG.shemeDragElem.cursorLambdaY;
        cMatrix.e = cNewMatrixX;
        cMatrix.f = cNewMatrixY;

        /** Приклееваем входящие в блок стрелки */
        schemeSVG.movieArrowsSpyBlock({x:cNewMatrixX, y:cNewMatrixY}, 'IN');
        /** Приклееваем исходящие из блока стрелки */
        schemeSVG.movieArrowsSpyBlock({x:cNewMatrixX, y:cNewMatrixY}, 'OUT');

        schemeSVG.shemeDragElem.isChanged = true; // были изменения
    },
    /**
     * Перемещение стрелок вместе с блоком
     * coord: {x: number, y: number}
     * arrowsType: string : IN|OUT
     * */
    movieArrowsSpyBlock(coord, arrowsType){ // coord{x:number, y:number}
        let cModelElement = Object.assign({}, schemeSVG.model.node.filter(item=> item.id == schemeSVG.shemeDragElem.idElement)[0]);
        if(cModelElement){
            if(arrowsType === 'IN' || arrowsType === 'OUT'){
                let iModelArrows = arrowsType === 'IN' ? cModelElement.arrowIdIn : arrowsType === 'OUT' ? cModelElement.arrowIdOut: null;
                if(iModelArrows && iModelArrows.length){
                    for(let arrowModelByFilter of iModelArrows){
                        let arrow = Object.assign({}, arrowModelByFilter)
                        let arrowModel = {};
                        arrowModel = schemeSVG.model.node.filter(item=> item.id == arrow.idNode)[0];
                        if(arrowModel){
                            let points = {};
                            points = Object.assign({},arrowModel.points.split(','));
                            let resultMAtrix = {
                                x1: points[0].split(' ')[0], y1:points[0].split(' ')[1],
                                x2: points[1].split(' ')[0], y2:points[1].split(' ')[1]
                            }

                            switch (arrow.vectorHandle){ // учитываем смещение относительно "handle" - ручки блока
                                case 'top':
                                    if(arrowsType === 'IN'){
                                        resultMAtrix.x2 = coord.x+50;
                                        resultMAtrix.y2 = coord.y+2;
                                    }
                                    if(arrowsType === 'OUT'){
                                        resultMAtrix.x1 = coord.x+50;
                                        resultMAtrix.y1 = coord.y+2;
                                    }
                                    break;
                                case 'right':
                                    if(arrowsType === 'IN'){
                                        resultMAtrix.x2 = coord.x + 99;
                                        resultMAtrix.y2 = coord.y + 50;
                                    }
                                    if(arrowsType === 'OUT') {
                                        resultMAtrix.x1 = coord.x + 99;
                                        resultMAtrix.y1 = coord.y + 50;
                                    }
                                    break;
                                case 'bottom':
                                    if(arrowsType === 'IN'){
                                        resultMAtrix.x2 = coord.x + 50;
                                        resultMAtrix.y2 = coord.y + 99;
                                    }
                                    if(arrowsType === 'OUT') {
                                        resultMAtrix.x1 = coord.x + 50;
                                        resultMAtrix.y1 = coord.y + 99;
                                    }
                                    break;
                                case 'left':
                                    if(arrowsType === 'IN'){
                                        resultMAtrix.x2 = coord.x;
                                        resultMAtrix.y2 = coord.y + 51;
                                    }
                                    if(arrowsType === 'OUT') {
                                        resultMAtrix.x1 = coord.x;
                                        resultMAtrix.y1 = coord.y + 51;
                                    }
                                    break;
                            }

                            let resultMatrixClone = Object.assign({}, resultMAtrix)
                            if(resultMatrixClone.x1 && resultMatrixClone.x2 && resultMatrixClone.y1 && resultMatrixClone.y2){
                                arrowModel.points = `${resultMatrixClone.x1} ${resultMatrixClone.y1},${resultMatrixClone.x2} ${resultMatrixClone.y2}`;
                                schemeSVG.spyArrow(
                                    arrow.idNode,
                                    arrowModel.points
                                );
                            }
                        }
                    }

                }
            }
        }
    },
    /** Перемещение стрелки - старт - нажатие мыши */
    onArrowMousedown(event){
        event.stopPropagation();
        event.preventDefault();
        let cElem = event.currentTarget;
        let cTransform  = cElem.getCTM();
        schemeSVG.shemeDragElem.eventMouse = event;
        schemeSVG.shemeDragElem.eventTarget = cElem;
        schemeSVG.shemeDragElem.arrowRect = {x:cTransform.e, y:cTransform.f}
        schemeSVG.shemeDragElem.arrowVectorStart = cElem.getAttribute('or-vector');

        let cid = schemeSVG.shemeDragElem.eventTarget.parentElement.id;
        if(schemeSVG.shemeDragElem.id !== cid){
            schemeSVG.shemeDragElem.id = cid;

            let newArrowModel = new schemeSVG.defaultModelArrowClass( `${event.clientX} ${event.clientY}`, cid );
            let cNewArrow = schemeSVG.generateArrow(newArrowModel);

            schemeSVG.shemeDragElem.arrowModel = newArrowModel;
            schemeSVG.shemeDragElem.arrowElement = cNewArrow;
            schemeSVG.schemePoligon.prepend(cNewArrow);

            schemeSVG.scheme.removeEventListener('mouseup', schemeSVG.onSchemeMouseUp);
            schemeSVG.scheme.addEventListener('mouseup', schemeSVG.onSchemeMouseUp)
            schemeSVG.scheme.removeEventListener('mousemove', schemeSVG.renderArrow);
            schemeSVG.scheme.addEventListener('mousemove', schemeSVG.renderArrow)
        }

    },
    /** Перемещение стрелки - окончание - Отпустили мышь */
    onArrowMouseUp(event){
        event.stopPropagation();
        let cBLOCK = event.currentTarget.parentElement;
        if(cBLOCK.id !== schemeSVG.shemeDragElem.id){
            let cElem = event.target;
            let cTransform  = cElem.getCTM();
            let newPoints = `${schemeSVG.shemeDragElem.arrowRect.x} ${schemeSVG.shemeDragElem.arrowRect.y},${cTransform.e } ${cTransform.f}`;
            schemeSVG.shemeDragElem.arrowModel.points = newPoints;
            schemeSVG.shemeDragElem.arrowElement.setAttributeNS(null,'points',newPoints);

            schemeSVG.shemeDragElem.arrowModel.idNodeIn = cBLOCK.id;
            let cModelNodes = Object.assign([], schemeSVG.model.node);

            schemeSVG.model.node = cModelNodes.map((item)=>{
                let cItemClone = Object.assign({},item)

                if(cItemClone.id === schemeSVG.shemeDragElem.id){ // информация о стрелке в блок "откуда"
                    cItemClone.arrowIdOut.push(
                        new schemeSVG.arrowIdByBLockClass(
                            schemeSVG.shemeDragElem.arrowModel.id,
                            schemeSVG.shemeDragElem.arrowVectorStart
                        )
                    );
                }

                if(cItemClone.id == cBLOCK.id){ // информация о стрелке в блок "куда"
                    cItemClone.arrowIdIn.push(
                        new schemeSVG.arrowIdByBLockClass(
                            schemeSVG.shemeDragElem.arrowModel.id,
                            cElem.getAttribute('or-vector')
                        )
                    );
                }
                return cItemClone;
            })
            schemeSVG.updateAllModels(schemeSVG.shemeDragElem.arrowModel, true)
        } else{
            if(schemeSVG.shemeDragElem.arrowElement){ schemeSVG.shemeDragElem.arrowElement.remove() };
        }
        schemeSVG.scheme.removeEventListener('mousemove', schemeSVG.renderArrow);
        schemeSVG.resetDragElem();
    },
    /** Отпустили мышь на схеме, но не handler(ручке) блкока */
    onSchemeMouseUp(event){
        event.stopPropagation();
        schemeSVG.scheme.removeEventListener('mousemove', schemeSVG.renderArrow);
        schemeSVG.scheme.removeEventListener('mouseup', schemeSVG.onSchemeMouseUp);
        if(schemeSVG.shemeDragElem.arrowElement){schemeSVG.shemeDragElem.arrowElement.remove()};
        schemeSVG.resetDragElem();
    },
    /** Очищаем таскаемый элемент */
    resetDragElem(){ schemeSVG.shemeDragElem = new schemeSVG.defaultModelDragElementClass(); }
}


    /** ИНИЦИАЛИЗАЦИЯ и проверка */
    if(
        document.getElementById('or-dashboard')
        && document.getElementById('or-scheme-container')
    ){ schemeSVG.init();}

})
